#!/usr/bin/perl -w

use POSIX qw /strftime/;

open(A, "> test_calls.txt");
for (1...15000){
    my $avg=time-rand(1000000);
    my $fr=strftime ("%Y-%m-%d %H:%M:%S", localtime($avg-rand(1000)));
    my $to=strftime ("%Y-%m-%d %H:%M:%S", localtime($avg+rand(1000)));
    print A ("FROM:$fr TO:$to \n");
}
close A;

use Time::Local;
use Time::Piece;
use Date::Calc qw(Date_to_Time);

open(A,"< /home/jam/test_calls.txt");
$duration = $calls = $min_start = $max_stop = 0;
while ($a=<A>){

#	FROM:2019-05-06 21:28:55 TO:2019-05-06 21:49:22		# в лог добавил секунды
#	FROM:2019-04-26 13:24:55 TO:2019-04-26 13:33:22

    $start = Time::Piece->strptime(substr($a,5,19), "%Y-%m-%d %H:%M:%S");	# парсим дату и время начала звонка
    $stop = Time::Piece->strptime(substr($a,28,19), "%Y-%m-%d %H:%M:%S");	# парсим дату и время окончания звока

    $start = Date_to_Time($start->strftime("%Y"), $start->strftime("%m"), $start->strftime("%d"), $start->strftime("%H"), $start->strftime("%M"), $start->strftime("%S"));	# конвертируем дату время начала звонка в секунды (для расчёта длительности звонка)
    $stop = Date_to_Time($stop->strftime("%Y"), $stop->strftime("%m"), $stop->strftime("%d"), $stop->strftime("%H"), $stop->strftime("%M"), $stop->strftime("%S"));		# конвертируем дату и время окончания звонка в секунды (для расчёта длительности звонка)

    if ($min_start == 0 || $min_start > $start) {
	$min_start = $start;	# дата и время начала самого первого звонка
    }
    if ($max_stop == 0 || $max_stop < $stop) {
	$max_stop = $stop;	# дата и время окончания самого последнего звонка
    }

    $duration += ($stop - $start);	# общее время тел. разговоров, sec
    $calls += 1;	# общее ко-во звонков
}

$period = $max_stop - $min_start;	# период времени (в сек.) в который состоялись все звонки
$period_in_days = $period/(24*3600);	# период времени (в днях) в который состоялись все звонки

$avg_call_time = $duration/$calls;	# среднее время одного тел. звонка, в сек.

$operator_calls_time = 8*3600;		# Общее время, которое один оператор будет тратить на обслуживание вызовов в смену (8 часов) в сек.
$operator_calls = $operator_calls_time / $avg_call_time;	# число вызовов, которое может обслужить за смену один оператор.

$calls_per_day = ($calls/$period_in_days) / 3;	# среднее количество поступающих звонков в смену (за 8 часов).

$operators = $calls_per_day / $operator_calls;		# необходимое кол-во операторов

if($operators > int($operators)){
    $operators = int($operators) + 1;	# округляем кол-во операторов в большую сторону
}

if($calls_per_day > int($calls_per_day)){
    $calls_per_day = int($calls_per_day) + 1;		# округляем кол-во звонков в большую сторону
}

print "\nзвонки поступали на протяжении количества дней = ".int($period_in_days)."\nвсего звонков поступило = ".$calls."\nсреднее время одного звонка (в сек.) составило = ".int($avg_call_time)."\n";
print "\nза смену оператор может обслужить вызовов = ".int($operator_calls)."\nколичество поступающих в смену звонков = ".int($calls_per_day+1)."\nминимально необходимое кол-во операторов = ".int($operators+1)."\n\n";

